let idToken; // Authentication token.

// Setup handles for authentication. When authenticated, call load().
async function init() {
    firebase.auth().onAuthStateChanged(
        async function (user) {
            if (user) {
                // User is signed in.
                idToken = await user.getIdToken();
                console.log(idToken);
                document.getElementById('username').innerHTML = user.displayName.split(' ')[0];
                document.getElementById('signin-section').style.visibility = 'hidden';
                document.getElementById('main-section').style.visibility = 'visible';
                document.getElementById('auth-menu').style.visibility = 'visible';
                load();
            } else {
                // User is signed out.
                // Initialize the FirebaseUI Widget using Firebase to allow signing in.
                const ui = new firebaseui.auth.AuthUI(firebase.auth());
                const uiConfig = {
                    signInFlow: 'popup',
                    signInOptions: [
                        // List of OAuth providers supported.
                        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                    ],
                    callbacks: {
                        signInSuccessWithAuthResult: () => false,
                    },
                };
                ui.start('#firebaseui-auth-container', uiConfig);
            }
        },
        function (error) {
            console.error(error);
        }
    );

    document.getElementById('signout').onclick = function () {
        firebase
            .auth()
            .signOut()
            .then(
                function () {
                    console.log('Signed Out');
                    location.reload();
                },
                function (error) {
                    console.error('Sign Out Error', error);
                }
            );
    };
}

async function load() {
    document.getElementById('showPortfolioChart').onclick = function () {
        if (document.getElementById('showPortfolioChart').checked) {
            document.getElementById('portfolioChart').style.visibility = 'visible';
            generatePositionChart(coins);
        } else {
            document.getElementById('portfolioChart').style.visibility = 'hidden';
        }
    };

    // This will be an array of objects that describe the coins to watch by name & symbol.
    let coins = await getCoins();
    // Create the HTML markup to have a visual bubble for each coin.
    generateBubbles(coins);

    // Setup each bubble to link to a tradingview chart.
    for (let i = 0; i < coins.length; i++) {
        document.getElementById(coins[i].symbol + 'Bubble').onclick = function () {
            window.open('https://www.tradingview.com/chart/?symbol=COINBASE%3A' + coins[i].symbol + 'USD');
        };
    }

    // For the portfolio view, determine what the initial investment was in order to calculate +/-.
    try {
        document.getElementById('initial_usd-balance').value = await getPersonalAmount('initial_usd');
    } catch (e) {}
    await refreshPersonalBalance(coins);

    //
    let coin = {};

    // Handle "Save changes" button within the Balance modal.
    document.getElementById('updateBalance').onclick = async function () {
        idToken = await firebase.auth().currentUser.getIdToken();
        // let newBalance = { balance: {} };
        // Cycle through coins and grab value.
        let balance = {};
        for (let i = 0; i < coins.length; i++) {
            if (document.getElementById(coins[i].symbol + '-balance').value == '') balance[coins[i].symbol] = 0;
            else balance[coins[i].symbol] = document.getElementById(coins[i].symbol + '-balance').value;
        }

        balance['initial_usd'] = document.getElementById('initial_usd-balance').value;

        await fetch('/balance', {
            method: 'POST',
            headers: {
                authorization: idToken,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ balance: balance }),
        });

        await refreshPersonalBalance(coins);
        await updateData(coin, coins);
        if (document.getElementById('showPortfolioChart').checked) generatePositionChart(coins);
    };

    // Handle "Save changes" button within the Coins modal.
    document.getElementById('coins-update').onclick = async function () {
        let coinInputExists = true;
        let coinCount = 0;
        // Cycle through coins by checking checking for the array number that is incremented when it's populated.
        while (coinInputExists) {
            let element = document.getElementById(coinCount + '-coins-name');
            if (typeof element != 'undefined' && element != null) coinCount++;
            else coinInputExists = false;
        }

        // Remove any coin that has its name/symbol deleted when saved.
        let newCoins = [];
        for (let i = 0; i < coinCount; i++) {
            if (document.getElementById(i + '-coins-name').value !== '' && document.getElementById(i + '-coins-symbol').value !== '') {
                newCoins.push({
                    name: document.getElementById(i + '-coins-name').value,
                    symbol: document.getElementById(i + '-coins-symbol').value,
                });
            }
        }

        await fetch('/coins', {
            method: 'POST',
            headers: {
                authorization: idToken,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ coins: newCoins }),
        });
        // Easier to just reload and clear things out.
        window.location.reload();
    };

    // Handle the refresh button.
    document.getElementById('refresh').onclick = async () => updateData(coin, coins);

    // Automatically refresh every 60 seconds.
    let sleepTimer = 60000;
    while (1) {
        let count = 60,
            timer = setInterval(function () {
                document.getElementById('updateTimer').innerHTML = count--;
                if (count == 1) clearInterval(timer);
            }, 1000);
        await updateData(coin, coins);
        if (document.getElementById('showPortfolioChart').checked) generatePositionChart(coins);
        await sleep(sleepTimer);
    }
}

async function refreshPersonalBalance(coins) {
    for (let i = 0; i < coins.length; i++) {
        let personalAmount = await getPersonalAmount(coins[i].symbol);
        if (personalAmount == -1) personalAmount = await getPersonalAmount(coins[i].symbol);
        if (personalAmount == undefined) personalAmount = 0;
        document.getElementById(coins[i].symbol + 'PersonalAmount').innerHTML = personalAmount + ' ' + coins[i].symbol;
        document.getElementById(coins[i].symbol + '-balance').value = personalAmount;
    }
}

// Generate a chart describing portfolio holdings by coin.
function generatePositionChart(coins) {
    let portChartLabels = [];
    let portChartData = [];
    let portChartBgColor = [];
    for (let i = 0; i < coins.length; i++) {
        // Labels
        portChartLabels.push(coins[i].name);

        // Data
        let price = parseFloat(
            document
                .getElementById(coins[i].symbol + 'Price')
                .innerHTML.replace(/,/g, '')
                .replace(/\$/g, '')
        );
        let quantity = parseFloat(document.getElementById(coins[i].symbol + 'PersonalAmount').innerHTML.split(' ')[0]);
        let value = (price * quantity).toFixed(2);
        portChartData.push(value);

        // Colors
        let element = document.getElementById(coins[i].symbol + 'Bubble');
        let style = window.getComputedStyle(element, '');
        let bgColor = style.getPropertyValue('background-color');
        portChartBgColor.push(bgColor);
    }
    new Chart(document.getElementById('portfolioChart'), {
        type: 'doughnut',
        data: {
            labels: portChartLabels,
            datasets: [
                {
                    label: 'Portfolio Positions',
                    backgroundColor: portChartBgColor,
                    data: portChartData,
                },
            ],
        },
        options: {
            title: {
                display: true,
                text: 'Portfolio Positions by Coin',
            },
        },
    });
}

// Try to retrieve user's configuration of coins to watch; if nothing specific, return a default list.
async function getCoins() {
    let result;
    try {
        result = await (await fetch('/coins', { method: 'GET', headers: { authorization: idToken } })).json();
    } catch (e) {}

    let coins = [];
    if (result !== undefined) coins = result;
    else {
        coins = [
            {
                name: 'Bitcoin',
                symbol: 'btc',
            },
            {
                name: 'Etherum',
                symbol: 'eth',
            },
            {
                name: 'Solana',
                symbol: 'sol',
            },
            {
                name: 'Cardano',
                symbol: 'ada',
            },
            {
                name: 'Chainlink',
                symbol: 'link',
            },
        ];
    }
    return coins;
}

// Update data for coins (e.g. price, balance, change).
async function updateData(coin, coins) {
    for (let i = 0; i < coins.length; i++) {
        let coinStat = await (await fetch('https://api.pro.coinbase.com/products/' + coins[i].symbol + '-USD/stats', { method: 'GET' })).json();

        lastPrice = parseFloat(coin[coins[i].symbol]);
        let currPrice = parseFloat(coinStat.last).toFixed(2);
        document.getElementById(coins[i].symbol + 'Price').innerHTML = '$' + formatPrice(currPrice);

        let personalAmount = document.getElementById(coins[i].symbol + 'PersonalAmount').innerHTML.split(' ')[0];

        let priceDelta = currPrice - lastPrice;
        if (lastPrice !== 0 && priceDelta !== 0) {
            if (priceDelta > 0) {
                document.getElementById(coins[i].symbol + 'Change').innerHTML = '+' + priceDelta.toFixed(2);
                document.getElementById(coins[i].symbol + 'PersonalChange').innerHTML = '+' + (priceDelta * personalAmount).toFixed(2);
                document.getElementById(coins[i].symbol + 'Change').style.color = 'green';
                document.getElementById(coins[i].symbol + 'PersonalChange').style.color = 'green';
            } else if (priceDelta < 0) {
                document.getElementById(coins[i].symbol + 'Change').innerHTML = priceDelta.toFixed(2);
                document.getElementById(coins[i].symbol + 'PersonalChange').innerHTML = (priceDelta * personalAmount).toFixed(2);
                document.getElementById(coins[i].symbol + 'Change').style.color = '#cc0000';
                document.getElementById(coins[i].symbol + 'PersonalChange').style.color = '#cc0000';
            }
        }

        coin[coins[i].symbol] = currPrice;
    }

    let totalChange = (totalValue = 0);

    for (let i = 0; i < coins.length; i++) {
        if (document.getElementById(coins[i].symbol + 'PersonalChange').innerHTML !== '--') {
            totalChange += parseFloat(document.getElementById(coins[i].symbol + 'PersonalChange').innerHTML);
        }
        let coinPrice = parseFloat(
            document
                .getElementById(coins[i].symbol + 'Price')
                .innerHTML.replace(/\$/, '')
                .replace(/,/, '')
        );
        let coinAmount = parseFloat(document.getElementById(coins[i].symbol + 'PersonalAmount').innerHTML);
        totalValue += coinPrice * coinAmount;
    }

    try {
        if (document.getElementById('initial_usd-balance').value > 0) {
            document.getElementById('portfolioProfitLoss').style.visibility = 'visible';
            let profitLoss = (totalValue - document.getElementById('initial_usd-balance').value).toFixed(2);
            document.getElementById('personalProfitLoss').style.color = profitLoss > 0 ? '#00ff80' : '#cc0000';
            document.getElementById('personalProfitLoss').innerHTML = '$' + formatPrice(profitLoss);
        }
    } catch (e) {}

    totalValue = '$' + formatPrice(totalValue.toFixed(2));
    document.getElementById('personalAmount').innerHTML = totalValue;
    if (totalChange !== 0) {
        totalChange = totalChange.toFixed(2);
        document.getElementById('personalChangeAmount').innerHTML = totalChange > 0 ? '+' + totalChange : totalChange;
        document.getElementById('personalChangeAmount').style.color = totalChange > 0 ? '#00ff80' : '#cc0000';
    }

    document.getElementById('lastUpdated').innerHTML = new Date().toLocaleTimeString();
}

// Create the HTML markup to have a visual bubble for each coin.
function generateBubbles(coins) {
    for (let i = 0; i < coins.length; i++) {
        document.getElementById('bubbleRow').innerHTML += `
	    <p id="${coins[i].symbol}Bubble" class="col coinBubble">
	        <span class="coinName">${coins[i].name}</span><br>
	        <span id="${coins[i].symbol}Price" class="coinPrice">null</span><br> 
	        <span id="${coins[i].symbol}Change" class="coinChange">--</span><br>
	        <span id="${coins[i].symbol}PersonalAmount" class="coinPersonalAmount">--</span><br>
	        <span id="${coins[i].symbol}PersonalChange" class="coinPersonalChange">--</span><br>
	    </p>
        `;

        // Assign a random color if not specified in CSS
        let element = document.getElementById(coins[i].symbol + 'Bubble');
        let style = window.getComputedStyle(element, '');
        let bgColor = style.getPropertyValue('background-color');
        if (bgColor == 'rgb(0, 0, 0)') {
            bgColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
            document.getElementById(coins[i].symbol + 'Bubble').style.background = bgColor;
        }

        document.getElementById('balance-modal-body').innerHTML += `
        <div class="input-group flex-nowrap mb-1">
            <span class="input-group-text balance-input-group-text" id="${coins[i].symbol}-balance-label">${coins[i].symbol}</span>
            <input id="${coins[i].symbol}-balance" type="text" class="form-control" placeholder="0.00000000" aria-label="${coins[i].symbol}" aria-describedby="${coins[i].symbol}-balance-label">
        </div>
        `;
    }
    for (let i = 0; i <= coins.length; i++) {
        if (i == coins.length) {
            document.getElementById('coins-modal-body').innerHTML += `
        <div class="input-group">
            <span class="input-group-text coins-input-group-text" id="${i}-coins-label">${i}</span>
            <input id="${i}-coins-name" type="text" aria-label="Name" class="form-control" placeholder="Name">
            <input id="${i}-coins-symbol" type="text" aria-label="Symbol" class="form-control" placeholder="Symbol">
        </div>
        `;
        } else {
            document.getElementById('coins-modal-body').innerHTML += `
        <div class="input-group">
            <span class="input-group-text coins-input-group-text" id="${i}-coins-label">${i}</span>
            <input id="${i}-coins-name" type="text" aria-label="Name" class="form-control" placeholder="Name" value="${coins[i].name}">
            <input id="${i}-coins-symbol" type="text" aria-label="Symbol" class="form-control" placeholder="Symbol" value="${coins[i].symbol}">
        </div>
        `;
        }
    }
    // Keep bubbles in pairs on mobile.
    if (coins.length % 2 !== 0) {
        document.getElementById('bubbleRow').innerHTML += `
        <p id="" class="col coinBubble placeholderBubble"></p>
        `;
    }
}

// Sleep function.
function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

// Try to retrieve user's configuration of coin balances.
async function getPersonalAmount(coinName) {
    try {
        let balance = await (await fetch('/balance', { method: 'GET', headers: { authorization: idToken } })).json();
        if (balance !== -1) return balance[coinName];
    } catch (e) {
        return 0;
    }
}

// Regular expression to add commas to amounts for visual purposes.
let formatPrice = (price) => price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');

window.onload = init;
