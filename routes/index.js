const express = require('express');
const router = express.Router();
const request = require('request');
const fs = require('fs');

// Firebase Admin SDK
const firebase = require('firebase-admin');
firebase.initializeApp();

const auth = firebase.auth();
const db = firebase.firestore();
const collection_name = 'coindeets';

(async () => {
    let project = await getProject();

    /* GET home page. */
    router.get('/', async function (req, res, next) {
        res.render('index', {
            title: 'Coindeets',
            service: process.env.K_SERVICE == undefined ? '???' : process.env.K_SERVICE,
            revision: process.env.K_REVISION == undefined ? '???' : process.env.K_REVISION,
            project: project === undefined ? '???' : project,
        });
    });
})();

/*
There are two main endpoints: /coins & /balance.
    - /coins is used to update what coins should be shown for a user if non-default.
    - /balance is used to configure a user's personal balance for each coin.

An example Firestore entry for a user with both configured:
coindeets (collection)
- [user_id] (document)
	- balance (map)
	    - [coinSymbol1]: "" (string)
	    - [coinSymbol2]: "" (string)
	- coins (array)
	    - 0 (map)
	        - name: "" (string)
	        - symbol: "" (string)
	    - 1 (map)
	        - name: "" (string)
	        - symbol: "" (string)
	- email: "" (string)

Useful Firestore documentation:
- https://firebase.google.com/docs/firestore/manage-data/add-data
- https://firebase.google.com/docs/firestore/query-data/get-data
*/
router.get('/coins', checkAuth, async (req, res) => {
    try {
        const user = await auth.verifyIdToken(req.headers.authorization);
        const response = await getUserDeets(user.user_id);
        res.status(200).json(response.coins);
    } catch (e) {
        res.status(200).json({ coins: 0 });
    }
});

router.post('/coins', checkAuth, async (req, res) => {
    if (!req.body.coins) {
        console.log('No coins received in request.');
        return res.status(500).json({ error: 'No coins received in request ' });
    }
    try {
        const user = await auth.verifyIdToken(req.headers.authorization);
        const body = {
            coins: req.body.coins,
        };
        const doc = db.collection(collection_name).doc(user.user_id);
        await doc.set(body, { merge: true });
        console.log(`New coins entry: ${doc.id}`);
        res.status(200).json(await getUserDeets(user.user_id));
    } catch (e) {
        console.log('Error: ' + e);
        res.status(400).json({ error: 'Unable to record coins' });
    }
});

router.get('/balance', checkAuth, async (req, res) => {
    try {
        const user = await auth.verifyIdToken(req.headers.authorization);
        const response = await getUserDeets(user.user_id);
        res.status(200).json(response.balance);
    } catch (e) {
        console.log('error');
        res.status(200).json({ balance: 0 });
    }
});

router.post('/balance', checkAuth, async (req, res) => {
    if (!req.body.balance) {
        console.log(req);
        console.log('No balance received in request.');
        return res.status(500).json({ error: 'No balance received in request ' });
    }
    try {
        const user = await auth.verifyIdToken(req.headers.authorization);
        const body = {
            balance: req.body.balance,
        };
        const doc = db.collection(collection_name).doc(user.user_id);
        await doc.set(body, { merge: true });
        console.log(`New balance entry: ${doc.id}`);
        res.status(200).json(await getUserDeets(user.user_id));
    } catch (e) {
        console.log('Error: ' + e);
        res.status(400).json({ error: 'Unable to record balance' });
    }
});

async function checkAuth(req, res, next) {
    const idToken = req.headers.authorization;
    if (!idToken) {
        console.log(`Error: No ID Token.`);
        return res.status(403).json({ error: 'Unauthorized' });
    }
    await auth
        .verifyIdToken(idToken)
        .then(() => {
            next();
        })
        .catch(function (error) {
            res.status(403).json({ error: error });
        });
}

async function getUserDeets(user_id) {
    try {
        const docRef = db.collection(collection_name).doc(user_id);
        const doc = await docRef.get();
        if (doc.exists) {
            console.log(doc.id, '=>', doc.data());
            return doc.data();
        } else {
            return -1;
        }
    } catch (e) {
        console.log(`Error getting balance: ${e}`);
        return -1;
    }
}

function getProject() {
    return new Promise((resolve, reject) => {
        const options = {
            url: 'http://metadata.google.internal/computeMetadata/v1/project/project-id',
            headers: {
                'Metadata-Flavor': 'Google',
            },
        };
        request.get(options, (error, response, body) => {
            if (!error && response.statusCode == 200) {
                return resolve(body);
            } else {
                return resolve();
            }
        });
    });
}

module.exports = router;
