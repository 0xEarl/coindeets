# Exit on unset variable.
set -u

# Ensure the script is executed from the repo root.
if [ ! -d "scripts" ] 
then
    echo "This should be run from the repostiory root and not inside the scripts folder." 
    exit 1
fi

# Ensure variables are loaded.
source app.config

# Deploy to Cloud Run
gcloud builds submit --tag gcr.io/$PROJECT_ID/$APP_NAME \
  --project $PROJECT_ID
gcloud beta run deploy $APP_NAME \
  --image gcr.io/$PROJECT_ID/$APP_NAME \
  --platform managed \
  --region $REGION \
  --allow-unauthenticated \
  --project $PROJECT_ID \
  --update-env-vars APP_NAME=$APP_NAME,COLLECTION_NAME=$COLLECTION_NAME \
